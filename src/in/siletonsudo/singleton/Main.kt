package `in`.siletonsudo.singleton

class FileResource constructor() {
    private val cacheManager: CacheManager = CacheManager.getInstance()
    fun doFileOperation() {
        println("Doing file operation")
        cacheManager.put("file", "this is some file operation")
        println("File operation completed with hashcode *${cacheManager.hashCode()}*")
    }
}

class NetworkResource constructor() {
    private val cacheManager: CacheManager = CacheManager.getInstance()
    fun doNetworkOperation() {
        println("Doing network operation")
        cacheManager.put("network", "this is some network operation")
        println("Network operation completed with hashcode *${cacheManager.hashCode()}*")
    }
}

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val cacheManager: CacheManager = CacheManager.getInstance()

        println("Initial CacheManager instance count is ${CacheManager.myInstancesCount}")

        println("")

        val networRes = NetworkResource()
        networRes.doNetworkOperation()
        println("CacheManager instance count is ${CacheManager.myInstancesCount} after network operation")

        println("")

        val fileRes = FileResource()
        fileRes.doFileOperation()
        println("CacheManager instance count is ${CacheManager.myInstancesCount} after file operation")

        println("")
        println("Main call cache manager hashcode *${cacheManager.hashCode()}*")

        println("File operation value: ${cacheManager.get("file")}")
        println("Network operation value: ${cacheManager.get("network")}")
        println("Main call cache manager hashcode after retrieving values *${cacheManager.hashCode()}*")

    }
}